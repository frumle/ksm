package ksm.scope.function

import ksm.core.base.Expr
import ksm.scope.MathScope
import ksm.core.function.Function

class FunctionDefinitions: MathScope.Element {

    companion object Key : MathScope.Key<FunctionDefinitions>

    override val key: MathScope.Key<*> get() = Key

    private val _definitions: MutableMap<Function, Expr> = mutableMapOf()

    val definitions: Map<Function, Expr> = _definitions

    fun Function.define(definition: Expr, redefine: Boolean = false){
        if(_definitions.contains(this) || redefine)
            _definitions[this] = definition
    }

}

val MathScope.definitions: FunctionDefinitions? get() = this[FunctionDefinitions.Key]