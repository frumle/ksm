package ksm.scope.function

import ksm.core.Variable
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.function.Function
import ksm.core.Vector
import ksm.scope.ExtendableMathScope
import ksm.scope.MathScope
import ksm.scope.variables.DeclarationInterceptor

class FunctionDefinitionScope: ExtendableMathScope() {

    inner class Interceptor: DeclarationInterceptor {

        override fun declareVariable(variable: Variable) = declare(variable)
        override fun declareVector(vector: Vector) = declare(vector)

        private fun declare(expr: Expr){
            if(_parameters.contains(expr).not()){
                _parameters.add(expr)
            }
        }

    }

    init {
        add(Interceptor())
    }

    private val _parameters = mutableListOf<Expr>()
    val parameters: Array<Expr> get() = _parameters.toTypedArray()

}

fun of(vararg parameters: Expr): Array<out Expr> {
    return parameters
}

fun MathScope.func(name: String, parameters: Array<out Expr>): Function {
    parameters.forEach {
        require(it is Variable || it is Vector){
            "Only a Variable or Vector can be a playground.function parameter."
        }
    }
    return Function(name, parameters, Type.Real)
}

fun MathScope.func(name: String, build: FunctionDefinitionScope.()-> Expr?): Function {
    val builder = FunctionDefinitionScope()
    val body = builder.build()
    val parameters = builder.parameters
    parameters.forEach {
        require(it is Variable || it is Vector){
            "Only a Variable or Vector can be a playground.function parameter."
        }
    }
    return Function(name, parameters, Type.Real).also {
        if(body != null){
            definitions?.apply {
                it.define(body, false)
            }
        }
    }
}

