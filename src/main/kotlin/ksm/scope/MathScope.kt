package ksm.scope

interface MathScope {

    interface Element {
        val key: Key<*>
    }

    interface Key<E: Element>

    operator fun <E: Element> get(key: Key<E>): E?

}
