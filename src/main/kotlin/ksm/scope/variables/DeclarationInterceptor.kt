package ksm.scope.variables

import ksm.core.Variable
import ksm.core.Vector
import ksm.scope.MathScope

interface DeclarationInterceptor: MathScope.Element {

    companion object Key: MathScope.Key<DeclarationInterceptor>

    override val key: MathScope.Key<*> get() = Key

    fun declareVariable(variable: Variable)
    fun declareVector(vector: Vector)

}

val MathScope.declarationInterceptor: DeclarationInterceptor? get() = this[DeclarationInterceptor]