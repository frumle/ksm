package ksm.scope


open class ExtendableMathScope internal constructor() : MathScope {

    private val elements = mutableMapOf<MathScope.Key<*>, Any>()

    fun <E: MathScope.Element> add(element: E) {
        elements[element.key] = element
    }

    override fun <E : MathScope.Element> get(key: MathScope.Key<E>): E? {
        @Suppress("UNCHECKED_CAST")
        return elements[key] as? E
    }

}

fun mathScope(configuration: ExtendableMathScope.()->Unit): MathScope {
    return ExtendableMathScope().apply { configuration(this) }
}
