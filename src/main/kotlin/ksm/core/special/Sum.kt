package ksm.core.special

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.Variable

class Sum (
    index: Variable,
    lowerBound: Expr,
    upperBound: Expr,
    body: Expr
) : CompositeExpr("sum", listOf(index, lowerBound, upperBound, body)) {

    init {
        require(index.type == Type.Integer)
        require(lowerBound.type == Type.Integer)
        require(upperBound.type == Type.Integer)
    }

    val index: Variable get() = children[0] as Variable
    val lowerBound get() = children[1]
    val upperBound get() = children[2]
    val body get() = children[3]

    override val type = body.type
    override fun toString() = "${tag}[$index: $lowerBound, $upperBound]($body)"
    override fun equals(other: Any?) = other is Sum && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()

    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 4)
        require(children[0] is Variable)
        return Sum(children[0] as Variable, children[1], children[2], children[3])
    }

}

//fun sum(lowerBound: Expr, upperBound: Expr, expr: (Variable)->Expr) : Sum {
//    Sum(lowerBound, upperBound, expr)
//}
//
//fun sum(lowerBound: Int, upperBound: Expr, expr: (Variable)->Expr) : Sum {
//    TODO()
//}
//
//fun sum(lowerBound: Expr, upperBound: Int, expr: (Variable)->Expr) : Sum {
//    TODO()
//}
//
//fun sum(lowerBound: Int, upperBound: Int, expr: (Variable)->Expr) : Sum {
//    TODO()
//}


