package ksm.core.special

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.utils.plusHash

class KroneckerDelta (i: Expr, j: Expr) : CompositeExpr("kronecker", listOf(i, j)) {

    init {
        require(i.type == Type.Integer && j.type == Type.Integer)
    }

    val i get() = children.first()
    val j get() = children.last()

    override val type: Type get() = Type.Integer
    override fun toString() = "delta($i, $j)"
    override fun equals(other: Any?) = other is KroneckerDelta && hasSameChildrenAs(other)
    override fun hashCode() = childrenHash().plusHash(tag.hashCode())
    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 2)
        return KroneckerDelta(children[0], children[1])
    }

}

fun delta(i: Expr, j: Expr) : Expr {
    return KroneckerDelta(i, j)
}