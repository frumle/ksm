package ksm.core.special

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.Variable

class Prod (
    index: Variable,
    lowerBound: Expr,
    upperBound: Expr,
    body: Expr
) : CompositeExpr("prod", listOf(index, lowerBound, upperBound, body)) {

    init {
        require(index.type == Type.Integer)
        require(lowerBound.type == Type.Integer)
        require(upperBound.type == Type.Integer)
    }

    val index get() = children[0]
    val lowerBound get() = children[1]
    val upperBound get() = children[2]
    val body get() = children[3]

    override val type = body.type
    override fun toString() = "${tag}[$index: $lowerBound, $upperBound]($body)"
    override fun equals(other: Any?) = other is Prod && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()

    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 4)
        require(children[0] is Variable)
        return Prod(children[0] as Variable, children[1], children[2], children[3])
    }

}


