package ksm.core

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import kotlin.math.max

class Multiplication (terms: List<Expr>) : CompositeExpr("mul", terms) {

    constructor(vararg terms: Expr) : this(terms.asList())

    init {
        require(terms.size > 1)
    }

    override val type : Type by lazy(LazyThreadSafetyMode.PUBLICATION) {
        children.maxByOrNull { it.type.order }?.type ?: Type.Real
    }

    override fun toString() : String {
        return children.joinToString(" * ") {
            if(it is Addition) "($it)" else it.toString()
        }
    }
    override fun equals(other: Any?) = other is Multiplication && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()
    override fun cloneWith(children: List<Expr>) = Multiplication(children)

}

fun multiply (a: Expr, b: Expr) : Expr {
    return Multiplication(listOf(a, b))
}

fun multiply (vararg terms: Expr) : Expr {
    return Multiplication(terms.toList())
}

fun multiply (terms: List<Expr>) : Expr {
    return Multiplication(terms)
}

operator fun Expr.times(e: Expr) : Expr {
    return multiply(this, e)
}

operator fun Expr.times(a: Int) : Expr {
    return this.times(Number.int(a))
}

operator fun Expr.times(a: Double) : Expr {
    return this.times(Number.real(a))
}

operator fun Expr.times(a: Float) : Expr {
    return this.times(Number.real(a))
}
