package ksm.core

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.base.forward

class Log (base: Expr, expr: Expr) : CompositeExpr("log", listOf(base, expr)) {

    val base get() = children[0]
    val expr = children[1]

    override val type by lazy(LazyThreadSafetyMode.PUBLICATION){
        maxOf(
            base.type.forward(Type.Real),
            expr.type.forward(Type.Real)
        )
    }
    override fun toString(): String {
        return "log($base, $expr)"
    }
    override fun equals(other: Any?) =  other is Log && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()
    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 2)
        return Log(children[0], children[1])
    }
}

fun log(base: Expr, exponent: Expr) : Expr {
    return Log(base, exponent)
}

fun log(exponent: Expr) : Expr {
    return Log(Number.E, exponent)
}

fun log10(exponent: Expr) : Expr {
    return Log(Number.int(10), exponent)
}