package ksm.core

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.base.forward

class Cosh (expr: Expr) : CompositeExpr("cosh", listOf(expr)) {

    val expr get() = children.first()

    override val type by lazy(LazyThreadSafetyMode.PUBLICATION){
        expr.type.forward(Type.Real)
    }
    override fun toString() = "$tag($expr)"
    override fun equals(other: Any?) = other is Cosh && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()

    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 1)
        return Cosh(children.first())
    }

}

fun cosh(arg: Expr) : Expr {
    return Cosh(arg)
}