package ksm.core.arithmetic

fun gcd(a: Int, b: Int): Int {
    return if(a > b) gcdImpl(b, a)
    else gcdImpl(a, b)
}

private fun gcdImpl(a: Int, b: Int): Int {
    if(a == 0) return b
    return gcdImpl(b % a, a)
}