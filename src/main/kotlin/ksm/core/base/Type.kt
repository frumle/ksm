package ksm.core.base

sealed class Type (val order: Int): Comparable<Type>{

    override fun compareTo(other: Type): Int {
        return order.compareTo(other.order)
    }

    object Integer : Type(1) {
        override fun toString() = "Integer"
    }
    object Rational : Type(2) {
        override fun toString() = "Integer"
    }
    object Real : Type(3){
        override fun toString() = "Real"
    }
    object Complex : Type(4){
        override fun toString() = "Complex"
    }
    object Any: Type(5){
        override fun toString() = "Any"
    }

    companion object {

        private fun typeOf(i: Int) : Type {
            return when(i){
                1 -> Integer
                2 -> Rational
                3 -> Real
                4 -> Complex
                else -> Any
            }
        }

    }

}

fun Type.isSubtypeOf(other: Type): Boolean {
    return order <= other.order
}

fun Type.forward(type: Type): Type {
    return if(order < type.order) type
    else this
}