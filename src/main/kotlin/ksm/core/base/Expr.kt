package ksm.core.base

interface Expr {

    companion object {
        val Any = object: Expr {
            override val type: Type get() = Type.Any
            override fun toString() = "?"
            override fun equals(other: Any?) = other is Expr
            override fun hashCode() = "?".hashCode()
        }
    }

    val type: Type
    override fun toString() : String
    override fun equals(other: Any?): Boolean
    override fun hashCode(): Int

}

