package ksm.core.base

import ksm.utils.hash
import ksm.utils.plusHash

abstract class CompositeExpr (
    val tag: String,
    val children: List<Expr>
) : Expr {

    protected fun hasSameChildrenAs(exprB: CompositeExpr) : Boolean {
        if(children.size != exprB.children.size)
            return false
        val ita = children.iterator()
        val itb = exprB.children.iterator()
        while(ita.hasNext()){
            if(ita.next() != itb.next()) return false
        }
        return true
    }

    protected fun childrenHash() : Int = children.hash()

    abstract fun cloneWith(children: List<Expr>): Expr

}