package ksm.core

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.base.forward

class Power (base: Expr, pow: Expr) : CompositeExpr("pow", listOf(base, pow)) {

    val base get() = children[0]
    val pow = children[1]


    override val type by lazy(LazyThreadSafetyMode.PUBLICATION){
        maxOf(
            base.type.forward(Type.Real),
            pow.type.forward(Type.Real)
        )
    }

    override fun toString(): String {
        val bs = if(base is CompositeExpr) "($base)" else base.toString()
        val ps = if(pow is CompositeExpr) "($pow)" else pow.toString()
        return "$bs^$ps"
    }
    override fun equals(other: Any?) =  other is Exponent && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()
    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 2)
        return Power(children[0], children[1])
    }
}

fun pow(base: Expr, exponent: Int) : Expr {
    return Power(base, Number.int(exponent))
}

fun pow(base: Expr, exponent: Expr) : Expr {
    return Power(base, exponent)
}

fun sqrt(base: Expr) : Expr {
    return Power(base, Number.rational(1, 2))
}