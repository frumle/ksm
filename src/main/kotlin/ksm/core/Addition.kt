package ksm.core

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import kotlin.math.max

class Addition (terms: List<Expr>) : CompositeExpr("add", terms) {

    constructor(vararg terms: Expr) : this(terms.asList())

    init {
        require(terms.size > 1)
    }

    override val type : Type by lazy(LazyThreadSafetyMode.PUBLICATION) {
        children.maxByOrNull { it.type.order }?.type ?: Type.Real
    }

    override fun toString(): String {
        return children.joinToString(" + ") {
            if(it is CompositeExpr && !(it is Multiplication)) "($it)" else it.toString()
        }
    }
    override fun equals(other: Any?) = other is Addition && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()
    override fun cloneWith(children: List<Expr>) = Addition(children)

}

fun add (a: Expr, b: Expr) : Expr {
    return if(a === Number.NULL && b === Number.NULL) Number.NULL
    else if (a === Number.NULL && b !== Number.NULL) b
    else if (a !== Number.NULL && b === Number.NULL) a
    else Addition(mutableListOf(a, b))
}

fun add(vararg terms: Expr) : Expr {
    return add(terms.asList())
}

fun add (terms: List<Expr>) : Expr {
    return if(terms.size > 1){
        Addition(terms.toMutableList())
    } else terms.firstOrNull() ?: Number.NULL
}

operator fun Expr.plus(e: Expr) : Expr {
    return add(this, e)
}

operator fun Expr.plus(a: Int) : Expr {
    return this.plus(Number.int(a))
}

operator fun Expr.plus(a: Double) : Expr {
    return this.plus(Number.real(a))
}

operator fun Expr.plus(a: Float) : Expr {
    return this.plus(Number.real(a))
}


fun subtract (a: Expr, b: Expr) : Expr {
    return if(a === Number.NULL && b === Number.NULL) Number.NULL
    else if (a === Number.NULL && b !== Number.NULL) multiply(Number.MINUS_ONE, b)
    else if (a !== Number.NULL && b === Number.NULL) a
    else Addition(mutableListOf(a, multiply(Number.MINUS_ONE, b)))
}

operator fun Expr.minus(e: Expr) : Expr {
    return subtract(this, e)
}

operator fun Expr.minus(a: Int) : Expr {
    return this.minus(Number.int(a))
}

operator fun Expr.minus(a: Double) : Expr {
    return this.minus(Number.real(a))
}

operator fun Expr.minus(a: Float) : Expr {
    return this.minus(Number.real(a))
}