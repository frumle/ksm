package ksm.core

import ksm.core.base.Expr
import ksm.core.base.Type

open class Variable (
    val name: String,
    override val type: Type
    ) : Expr {

    override fun toString(): String = name

    override fun equals(other: Any?): Boolean {
        return other is Variable && other.name == name && other.type == type
    }

    override fun hashCode(): Int {
        return 31 * name.hashCode() + type.hashCode()
    }

}
