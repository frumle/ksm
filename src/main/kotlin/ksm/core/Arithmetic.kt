package ksm.core

import ksm.core.arithmetic.gcd
import ksm.core.base.Type
import kotlin.math.abs

fun add(a: Number, b: Number): Number {
    // Call corresponding operation via type specialization
    return when(maxOf(a.type, b.type)){
        Type.Integer -> add(a.toInteger(), b.toInteger())
        Type.Rational -> add(a.toRational(), b.toRational())
        Type.Real -> add(a.toReal(), b.toReal())
        Type.Complex -> add(a.toComplex(), b.toComplex())
        Type.Any -> throw Exception("Arithmetic operation type inference error: ($a + $b).")
    }
}

fun multiply(a: Number, b: Number): Number {
    return when(maxOf(a.type, b.type)){
        Type.Integer -> multiply(a.toInteger(), b.toInteger())
        Type.Rational -> multiply(a.toRational(), b.toRational())
        Type.Real -> multiply(a.toReal(), b.toReal())
        Type.Complex -> multiply(a.toComplex(), b.toComplex())
        Type.Any -> throw Exception("Arithmetic operation type inference error: ($a * $b).")
    }
}

fun divide(a: Number, b: Number): Number {
    return when(maxOf(a.type, b.type)){
        Type.Integer -> divide(a.toInteger(), b.toInteger())
        Type.Rational -> divide(a.toRational(), b.toRational())
        Type.Real -> divide(a.toReal(), b.toReal())
        Type.Complex -> divide(a.toComplex(), b.toComplex())
        Type.Any -> throw Exception("Arithmetic operation type inference error: ($a / $b).")
    }
}


private fun add(a: Number.IntegerNumber, b: Number.IntegerNumber): Number {
    return Number.int(a.value + b.value)
}

private fun add(a: Number.RationalNumber, b: Number.RationalNumber): Number {
    val gcdAB = gcd(a.denominator, b.denominator)
    val gcmAB = abs(a.denominator * b.denominator) / gcdAB
    val fa = gcmAB / a.denominator
    val fb = gcmAB / b.denominator
    return Number.RationalNumber(a.numerator * fa + b.numerator * fb, gcdAB).reduced()
}

private fun add(a: Number.RealNumber, b: Number.RealNumber): Number {
    return Number.real(a.value + b.value)
}

private fun add(a: Number.ComplexNumber, b: Number.ComplexNumber): Number {
    return Number.complex(a.real + b.real, a.imaginary + b.imaginary)
}


private fun multiply(a: Number.IntegerNumber, b: Number.IntegerNumber): Number {
    return Number.int(a.value * b.value)
}

private fun multiply(a: Number.RationalNumber, b: Number.RationalNumber): Number {
    return Number.RationalNumber(
        a.numerator * b.numerator,
        a.denominator * b.denominator
    ).reduced()
}

private fun multiply(a: Number.RealNumber, b: Number.RealNumber): Number {
    return Number.real(a.value * b.value)
}

private fun multiply(a: Number.ComplexNumber, b: Number.ComplexNumber): Number {
    return Number.complex(
        a.real * b.real - a.imaginary * b.imaginary,
        a.real * b.imaginary + a.imaginary * b.real
    )
}


private fun divide(a: Number.IntegerNumber, b: Number.IntegerNumber): Number {
    return Number.real(a.value.toDouble() / b.value)
}

private fun divide(a: Number.RationalNumber, b: Number.RationalNumber): Number {
    return Number.RationalNumber(
        a.numerator * b.denominator,
        a.denominator * b.numerator
    ).reduced()
}

private fun divide(a: Number.RealNumber, b: Number.RealNumber): Number {
    return Number.real(a.value / b.value)
}

private fun divide(a: Number.ComplexNumber, b: Number.ComplexNumber): Number {
    val r = a.real * b.real + a.imaginary * b.imaginary
    val i = - a.real * b.imaginary + a.imaginary * b.real
    val otherAbs = b.real * b.real + b.imaginary * b.imaginary
    return Number.complex(r / otherAbs, i / otherAbs)
}
