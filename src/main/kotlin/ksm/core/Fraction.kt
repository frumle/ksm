package ksm.core

import ksm.core.base.*
import ksm.utils.formatInParentheses

class Fraction (num: Expr, denom: Expr) : CompositeExpr("frac", listOf(num, denom)) {

    init {
        require(denom != Number.NULL) {
            "Denominator of the Fraction should be non zero."
        }
    }

    val numerator get() = children[0]
    val denominator get() = children[1]

    override val type by lazy(LazyThreadSafetyMode.PUBLICATION) {
        val nType = num.type
        val dType = denom.type
        if (nType.isSubtypeOf(Type.Rational) && dType.isSubtypeOf(Type.Rational))
            Type.Rational
        else
            maxOf(nType, dType)
    }

    override fun toString(): String {
        return "${numerator.formatInParentheses()}/${denominator.formatInParentheses()}"
    }
    override fun equals(other: Any?) = other is Fraction && hasSameChildrenAs(other)
    override fun hashCode() = 31 * childrenHash() + tag.hashCode()
    override fun cloneWith(children: List<Expr>): Expr {
        require(children.size == 2)
        return Fraction(children[0], children[1])
    }

}

fun frac (num: Expr, denom: Expr) : Expr {
    return if(num === Number.NULL) Number.NULL
    else if (denom === Number.ONE) num
    else Fraction(num, denom)
}

operator fun Expr.div(e: Expr) : Expr {
    return frac(this, e)
}

operator fun Expr.div(a: Int) : Expr {
    return this.div(Number.int(a))
}

operator fun Expr.div(a: Double) : Expr {
    return this.div(Number.real(a))
}

operator fun Expr.div(a: Float) : Expr {
    return this.div(Number.real(a))
}
