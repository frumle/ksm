package ksm.core.function

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.Variable
import ksm.core.array.NdArray
import ksm.core.array.dim
import ksm.utils.plusHash

typealias Parameters = Array<out Expr>

open class Function(
    val name: String,
    val parameters: Parameters,
    val type: Type
) {

    operator fun invoke(vararg args: Expr) = FunctionValue(this, args)

    override fun toString(): String {
        return "$name(${parameters.joinToString(",")})"
    }

    override fun equals(other: Any?): Boolean {
        return other is Function &&
                name == other.name &&
                parameters.size == other.parameters.size &&
                parameters.compareTypesAndDimensions(other.parameters)
    }

    override fun hashCode(): Int {
        var hash = name.hashCode()
        parameters.forEach {
            hash = hash.plusHash(it.type.hashCode())
        }
        return hash
    }

}

class FunctionDerivative(
    val function: Function,
    val orders: Map<Variable, Int>
): Function(
    function.derivativeNameFor(orders),
    function.parameters,
    function.type
){

    companion object {

        fun Function.derivativeNameFor(orders: Map<Variable, Int>): String {
            return StringBuilder().apply {
                append('d')
                val order = orders.values.reduce { acc, i -> acc + i }
                if(order > 1)
                    append(order)
                append(name)
                append('/')
                orders.forEach {
                    append('d')
                    append(it.key.toString())
                    if(it.value > 1){
                        append('^')
                        append(it.value.toString())
                    }
                }
            }.toString()
        }

    }

    override fun equals(other: Any?): Boolean {
        return if(other is FunctionDerivative && function == other.function){
            val parametersMap = function.parameters.zip(other.function.parameters).toMap()
            val mappedOrders = orders.mapKeys { parametersMap[it.key] }
            return mappedOrders == other.orders
        } else false
    }

    override fun hashCode(): Int {
        return function.hashCode().plusOrdersHash(orders)
    }

}

class FunctionValue(
    val function: Function,
    val arguments: Array<out Expr>
) : CompositeExpr(function.name, arguments.asList()) {

    override val type = function.type

    init {
        verifyArguments(arguments)
    }

    override fun toString(): String {
        return StringBuilder().apply {
            append(function.name)
            append("(")
            arguments.forEach {
                append(it.toString())
                append(",")
            }
            if(arguments.isNotEmpty())
                deleteCharAt(lastIndex)
            append(")")
        }.toString()
    }

    override fun equals(other: Any?): Boolean {
        return if(other is FunctionValue && function == other.function){
             arguments.contentEquals(other.arguments)
        } else false
    }

    override fun hashCode(): Int {
        return function.hashCode().plusParametersHash(arguments)
    }

    override fun cloneWith(children: List<Expr>): Expr {
        return FunctionValue(function, children.toTypedArray())
    }

    private fun verifyArguments(args: Array<out Expr>){
        require(args.size == function.parameters.size){
            "Wrong arguments number passed to ${function.name}: " +
                    "expected ${function.parameters.size}, passed  ${args.size}.\n" +
                    "Declared parameters: (${function.parameters.joinToString { it.toString() }}) \n" +
                    "Passed arguments   : (${args.joinToString { it.toString() }})"
        }
        args.forEachIndexed{ index, argument ->
            val parameter = function.parameters[index]

            val sameDimension = parameter.dim != argument.dim
            val compatibleType = argument.type.compatibleWith(parameter.type)

            require(sameDimension && compatibleType){
                "Wrong argument passed to ${function.name}: \n" +
                        "Argument index: $index \n" +
                        "Expected: type = ${parameter.type}, dim = ${parameter.dim} \n" +
                        "Passed: type = ${argument.type}, dim = ${argument.dim}."
            }
        }
    }

}


// Utils
private fun Parameters.compareTypesAndDimensions(
    parameters: Parameters,
): Boolean {
    forEachIndexed { i, p ->
        val op = parameters[i]
        if(p.type != op.type || p.dim != op.dim)
            return false
    }
    return true
}

private fun Int.plusParametersHash(
    parameters: Parameters
): Int {
    var hash = this
    parameters.forEach {
        hash = hash.plusHash(it.type.hashCode()).plusHash(it.dim)
    }
    return hash
}

private fun Int.plusOrdersHash(
    orders: Map<Variable, Int>
): Int {
    var hash = this
    orders.forEach {
        hash = hash.plusHash(it.key.type.hashCode())
            .plusHash(it.key.dim)
            .plusHash(it.value)
    }
    return hash
}

private fun Type.compatibleWith(type: Type): Boolean {
    return this.order >= type.order
}

val Expr.dim get() = if(this is NdArray) dim else 0