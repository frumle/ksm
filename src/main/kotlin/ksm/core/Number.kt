package ksm.core

import ksm.core.arithmetic.gcd
import ksm.core.base.Expr
import ksm.core.base.Type
import kotlin.math.abs

sealed class Number private constructor (
    value: String,
    override val type: Type
) : Expr {

    class IntegerNumber (val value: Int) : Number(
        "int($value)", Type.Integer
    ) {
        override fun toString() = value.toString()
        override fun equals(other: Any?) = other is IntegerNumber && other.value == value
        override fun hashCode() = 31 * value.hashCode() + type.hashCode()
    }

    class RationalNumber (val numerator: Int, val denominator: Int) : Number(
        "rational($numerator, $denominator)", Type.Real
    ) {
        override fun toString() = "($numerator/$denominator)"
        override fun equals(other: Any?) =
            other is RationalNumber &&
                    other.numerator == numerator &&
                    other.denominator == denominator
        override fun hashCode() = 31 * (31 * numerator.hashCode() + denominator.hashCode()) + type.hashCode()
    }

    class RealNumber (val value: Double) : Number(
        "real($value)", Type.Real
    ) {
        override fun toString() = value.toString()
        override fun equals(other: Any?) = other is RealNumber && other.value == value
        override fun hashCode() = 31 * value.hashCode() + type.hashCode()
    }

    class ComplexNumber (val real: Double, val imaginary: Double) : Number(
        "complex($real + ${imaginary}i)", Type.Complex
    ) {
        override fun toString() = "($real + ${imaginary}i)"
        override fun equals(other: Any?) =
                other is ComplexNumber &&
                other.real == real &&
                other.imaginary == imaginary
        override fun hashCode() = 31 * (31 * real.hashCode() + imaginary.hashCode()) + type.hashCode()
    }

    companion object {

        val ONE = IntegerNumber(1)
        val NULL = IntegerNumber(0)
        val MINUS_ONE = IntegerNumber(-1)
        val I = ComplexNumber(0.0, 1.0)
        val PI = RealNumber(kotlin.math.PI)
        val E = RealNumber(kotlin.math.E)

        fun int(value: Int) : IntegerNumber {
            return when (value){
                0 -> NULL
                1 -> ONE
                -1 -> MINUS_ONE
                else -> IntegerNumber(value)
            }
        }

        fun int(value: Long) : IntegerNumber {
            return when (value){
                0L -> NULL
                1L -> ONE
                -1L -> MINUS_ONE
                else -> IntegerNumber(value.toInt())
            }
        }


        fun rational(numerator: Int, denominator: Int) : Number {
            if(denominator == 0) throw IllegalArgumentException("Division by zero.")
            if(abs(denominator) == 1) return int(denominator * numerator)
            return RationalNumber(numerator, denominator)
        }

        fun real(value: Float): Number {
            return when (value){
                0.0f -> NULL
                1.0f -> ONE
                -1.0f -> MINUS_ONE
                else -> RealNumber(value.toDouble())
            }
        }

        fun real(value: Double): Number {
            return when (value){
                0.0 -> NULL
                1.0 -> ONE
                -1.0 -> MINUS_ONE
                else -> RealNumber(value)
            }
        }

        fun im(value: Double): Number {
            return when(value){
                0.0 -> NULL
                1.0 -> I
                else -> ComplexNumber(0.0, value)
            }
        }

        fun complex(realPart: Int, imaginaryPart: Int): Number {
            if(imaginaryPart == 0) return real(realPart.toDouble())
            if(realPart == 0) return im(imaginaryPart.toDouble())
            return ComplexNumber(realPart.toDouble(), imaginaryPart.toDouble())
        }

        fun complex(realPart: Int, imaginaryPart: Double): Number {
            if(imaginaryPart == 0.0) return real(realPart.toDouble())
            if(realPart == 0) return im(imaginaryPart)
            return ComplexNumber(realPart.toDouble(), imaginaryPart)
        }

        fun complex(realPart: Double, imaginaryPart: Int): Number {
            if(imaginaryPart == 0) return real(realPart)
            if(realPart == 0.0) return im(imaginaryPart.toDouble())
            return ComplexNumber(realPart, imaginaryPart.toDouble())
        }

        fun complex(realPart: Double, imaginaryPart: Double): Number {
            if(imaginaryPart == 0.0) return real(realPart)
            if(realPart == 0.0) return im(imaginaryPart)
            return ComplexNumber(realPart, imaginaryPart)
        }

    }

}

operator fun Number.plus(other: Number): Expr {
    return add(this, other)
}

operator fun Number.minus(b: Number): Expr {
    return add(this, multiply(Number.MINUS_ONE, b))
}

operator fun Number.times(b: Number): Expr {
    return multiply(this, b)
}

operator fun Number.div(b: Number): Expr {
    return frac(this, b)
}

fun Number.RationalNumber.reduced(): Number {
    val gcdND = gcd(numerator, denominator)
    return if(gcdND == 1) this
    else Number.rational(numerator/gcdND, denominator/gcdND)
}

fun Number.ComplexNumber.abs(): Number {
    return Number.real(real * real + imaginary * imaginary)
}

fun Number.ComplexNumber.conjugate(): Number.ComplexNumber {
    return Number.ComplexNumber(real, -imaginary)
}

fun Number.toInteger() : Number.IntegerNumber {
    return when(this){
        is Number.IntegerNumber -> this
        is Number.RationalNumber -> throw Exception("Casting real number to integer number.")
        is Number.RealNumber -> throw Exception("Casting real number to integer number.")
        is Number.ComplexNumber -> throw Exception("Casting complex number to integer number.")
    }
}

fun Number.toRational() : Number.RationalNumber {
    return when(this){
        is Number.IntegerNumber -> Number.RationalNumber(this.value, 1)
        is Number.RationalNumber -> this
        is Number.RealNumber -> throw Exception("Casting a real number to a rational number.")
        is Number.ComplexNumber -> throw Exception("Casting a complex number to a real number.")
    }
}

fun Number.toReal() : Number.RealNumber {
    return when(this){
        is Number.IntegerNumber -> Number.RealNumber(value.toDouble())
        is Number.RationalNumber -> Number.RealNumber(numerator.toDouble() / denominator)
        is Number.RealNumber -> this
        is Number.ComplexNumber -> throw Exception("Casting complex number to real number.")
    }
}

fun Number.toComplex() : Number.ComplexNumber {
    return when(this){
        is Number.IntegerNumber -> Number.ComplexNumber(value.toDouble(), 0.0)
        is Number.RationalNumber -> Number.ComplexNumber(toReal().value, 0.0)
        is Number.RealNumber -> Number.ComplexNumber(value, 0.0)
        is Number.ComplexNumber -> this
    }
}
