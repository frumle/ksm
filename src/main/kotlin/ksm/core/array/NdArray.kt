package ksm.core.array

import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.utils.hash

typealias Shape = Array<Expr>

abstract class NdArray(
    val name: String,
    override val type: Type,
    val shape: Shape
): Expr {

    init {
        shape.forEach {
            if(it.type != Type.Integer)
                throw Throwable("NdArray: wrong dimension type: ${it.type}, expected: ${Type.Integer}.")
        }
    }

    override fun toString(): String = name

    override fun equals(other: Any?): Boolean {
        return other is NdArray &&
                other.type == this.type &&
                other.shape.contentEquals(this.shape)
    }

    override fun hashCode(): Int {
        val hash = 31 * shape.hash() + type.hashCode()
        return 31 * hash + name.hashCode()
    }

    abstract fun get(vararg at: Expr): Expr

}


val NdArray.dim: Int get() = shape.size
