package ksm.core.array

import ksm.core.base.Expr
import ksm.core.base.Type

class VariableArray(
    name: String,
    type: Type,
    dim: Expr
): NdArray(name, type, arrayOf(dim)) {

    override fun get(vararg at: Expr): Expr {
        TODO("Not yet implemented")
    }

}