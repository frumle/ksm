package ksm.core.array

import ksm.core.base.Expr

typealias Indices = Array<Expr>

inline val Indices.i1 get() = get(1)
inline val Indices.i2 get() = get(2)
inline val Indices.i3 get() = get(3)
inline val Indices.i4 get() = get(4)
inline val Indices.i5 get() = get(5)
inline val Indices.i6 get() = get(6)
inline val Indices.i7 get() = get(7)
inline val Indices.i8 get() = get(8)
inline val Indices.i9 get() = get(9)
inline val Indices.i10 get() = get(10)