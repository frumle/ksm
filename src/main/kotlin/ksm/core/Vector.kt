package ksm.core

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.utils.plusHash

open class Vector (
    val name: String,
    override val type: Type,
    val size: Expr,
) : Expr {

    init {
        require(size.type == Type.Integer) {
            "Size of the VectorVariable should be of type Type.Integer."
        }
    }

    override fun toString(): String = name

    override fun equals(other: Any?): Boolean {
        return if(other is Vector){
            name == other.name &&
            type == other.type &&
            size == other.size
        } else false
    }

    override fun hashCode(): Int {
        return name.hashCode()
            .plusHash(type.hashCode())
            .plusHash(size.hashCode())
    }

    private fun component(index: Expr) : Component = Component(this, index)

    class Component (
        val vector: Vector,
        val index: Expr
    ) : Variable("${vector}_${index}", vector.type) {

        init {
            require(index.type == Type.Integer) {
                "Index of the VectorVariable component should be of type Type.Integer."
            }
        }

        override val type: Type
            get() = vector.type

        override fun toString(): String {
            return when(index){
                is CompositeExpr -> "${vector}[${index}]"
                else -> "${vector}[${index}]"
            }
        }

    }

}


fun vec(name: String, type: Type, size: Variable) : Vector {
    return Vector(name, type, size)
}

fun vec(name: String, type: Type, size: Int) : Vector {
    return Vector(name, type, Number.int(size))
}

fun vec(name: String, type: Type, size: String) : Vector {
    return Vector(name, type, Variable(size, Type.Integer))
}


operator fun Vector.get(i: Int) : Vector.Component {
    return Vector.Component(this, Number.int(i))
}

operator fun Vector.get(expr: Expr) : Vector.Component {
    return Vector.Component(this, expr)
}

operator fun Vector.get(i: String) : Vector.Component {
    return Vector.Component(this, Variable(i, Type.Integer))
}

operator fun Vector.get(i: Char) : Vector.Component {
    return Vector.Component(this, Variable(i.toString(), Type.Integer))
}