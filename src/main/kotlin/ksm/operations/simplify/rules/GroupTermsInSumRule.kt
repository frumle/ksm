package ksm.operations.simplify.rules

import ksm.core.base.Expr
import ksm.operations.simplify.Simplify

class GroupTermsInSumRule : Simplify.Rule {

    override fun apply(expr: Expr): Expr {
        group(expr)
        return expr
    }

    private fun group(mutable: Expr) {
//        if(mutable.expr is Sum){
//            val childrenCountMap = mutableMapOf<MutableExpr, Number<*>>()
//            var maxCount: Number<*> = Number.Null
//
//            childrenCountMap.clear()
//            for(c in mutable.children){
//                val (num, expr) = extractNumber(c)
//                val n = (childrenCountMap[expr] ?: Number.Null) + num
//                childrenCountMap[expr] = n
//                maxCount = max(maxCount, n)
//            }
//
//            if(maxCount !== Number.One){
//                val newChildren = mutableListOf<MutableExpr>()
//                for( (e, n) in childrenCountMap){
//                    val newExpr = MutableExpr.from(prod(Number.int(n), e.expr))
//                    newExpr.children = e.children
//                    newChildren += newExpr
//                }
//                mutable.children = newChildren
//            }
//
//        }
//        for(c in mutable.children) group(c)
    }

//    private fun extractNumber(m: MutableExpr) : Pair<Number<*>, MutableExpr> {
//        return if(m.expr is Product){
//            var number : Number<*> = Number.Null
//            val cit = m.children.iterator()
//            while (cit.hasNext()){
//                val c = cit.next()
//                if(c.expr is Number<*>){
//                    cit.remove()
//                    number *= (c.expr as Number<*>)
//                }
//            }
//            if(number != Number.Null || m.children.size == 0){
//                m.children.add(0, MutableExpr.from(number))
//            }
//            Pair(Number.One, m)
//        } else Pair(Number.One, m)
//    }


//    private fun group(mutable: MutableExpr) {
//        if(mutable.expr is Sum){
//            val childrenCountMap = mutableMapOf<MutableExpr, Int>()
//            var maxCount = 0
//
//            childrenCountMap.clear()
//            for(c in mutable.children){
//                val n = (childrenCountMap[c] ?: 0) + 1
//                childrenCountMap[c] = n
//                maxCount = max(maxCount, n)
//            }
//
//            if(maxCount > 1){
//                val newChildren = mutableListOf<MutableExpr>()
//                for( (e, n) in childrenCountMap){
//                    val newExpr = MutableExpr.from(prod(Number.int(n), e.expr))
//                    newExpr.children = e.children
//                    newChildren += newExpr
//                }
//                mutable.children = newChildren
//            }
//
//        }
//        for(c in mutable.children) group(c)
//    }

}
