package ksm.operations.simplify.rules

import ksm.core.base.Expr
import ksm.operations.simplify.Simplify

class CanonicalFormRule : Simplify.Rule {

    override fun apply(expr: Expr): Expr {
        return prettifyProducts(expr)
    }

    private fun prettifyProducts(expr: Expr) : Expr {

        return expr
    }

}
