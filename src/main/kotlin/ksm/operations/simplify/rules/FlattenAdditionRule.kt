package ksm.operations.simplify.rules

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.Addition
import ksm.operations.simplify.Simplify

class FlattenAdditionRule : Simplify.Rule {

    override fun apply(expr: Expr): Expr {
        return if(expr is Addition){
            expr.cloneWith(flattenAddition(expr))
        } else flattenNonAddition(expr)
    }

    private fun flattenAddition(expr: Addition) : List<Expr> {
        return mutableListOf<Expr>().apply {
            for (child in expr.children) {
                if(child is Addition) this += flattenAddition(child)
                else this += flattenNonAddition(child)
            }
        }
    }

    private fun flattenNonAddition(expr: Expr) : Expr {
        return if(expr is CompositeExpr){
            val flattenChildren = mutableListOf<Expr>()
            for(child in expr.children){
                flattenChildren += if(child is Addition){
                    child.cloneWith(flattenAddition(child))
                } else flattenNonAddition(child)
            }
            expr.cloneWith(flattenChildren)
        } else expr
    }

}
