package ksm.operations.simplify.rules

import ksm.core.base.CompositeExpr
import ksm.core.base.Expr
import ksm.core.Multiplication
import ksm.operations.simplify.Simplify

class FlattenMultiplicationRule : Simplify.Rule {

    override fun apply(expr: Expr): Expr {
        return if(expr is Multiplication){
            expr.cloneWith(flattenMultiplication(expr))
        } else flattenNonMultiplication(expr)
    }

    private fun flattenMultiplication(expr: Multiplication) : List<Expr> {
        return mutableListOf<Expr>().apply {
            for (child in expr.children) {
                if(child is Multiplication) this += flattenMultiplication(child)
                else this += flattenNonMultiplication(child)
            }
        }
    }

    private fun flattenNonMultiplication(expr: Expr) : Expr {
        return if(expr is CompositeExpr){
            val flattenChildren = mutableListOf<Expr>()
            for(child in expr.children){
                flattenChildren += if(child is Multiplication){
                    child.cloneWith(flattenMultiplication(child))
                } else flattenNonMultiplication(child)
            }
            expr.cloneWith(flattenChildren)
        } else expr
    }

}
