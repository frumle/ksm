package ksm.operations.simplify.rules

import ksm.core.base.Expr
import ksm.operations.simplify.Simplify

class LexicographicalOrderRule : Simplify.Rule {

    override fun apply(expr: Expr): Expr {
        introduceOrder(expr)
        return expr
    }

    private fun introduceOrder(expr: Expr) {
//        with(expr){
//            if(this is CompositeExpr){
//                children.forEach { c -> introduceOrder(c) }
//                children.sortWith { c1, c2 -> c1.toString().compareTo(c2.toString()) }
//            }
//        }
    }

}
