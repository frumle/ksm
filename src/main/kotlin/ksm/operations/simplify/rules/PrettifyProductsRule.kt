package ksm.operations.simplify.rules

import ksm.core.base.Expr
import ksm.operations.simplify.Simplify

class PrettifyProductsRule : Simplify.Rule {

    override fun apply(expr: Expr): Expr {
        return prettifyProducts(expr)
    }

    private fun prettifyProducts(expr: Expr) : Expr {
//        with(expr){
//            if(this !is CompositeExpr) return this
//
//            for(i in children.indices)
//                children[i] = prettifyProducts(children[i])
//
//            var result = this
//            if(this is Multiplication){
//                if(children.containsZero()) return Number.NULL
//                children.removeOnes()
//                val minusOnesCount = children.removeMinusOnes()
//                if(minusOnesCount % 2 != 0) children.add(0, Number.MINUS_ONE)
//                result = when (children.size) {
//                    0 -> Number.NULL
//                    1 -> children.first()
//                    else -> this
//                }
//            }
//
//            return result
//        }
        return expr
    }

}
