package ksm.operations.simplify

import ksm.core.base.Expr
import ksm.operations.simplify.rules.*

class Simplify {

    data class Options (val rules: List<Rule>, val orderRules: List<Rule>) {

        companion object {
            val Default = Options(
                rules = listOf(
                    FlattenMultiplicationRule(),
                    PrettifyProductsRule(),
                    FlattenAdditionRule(),
                    NumbersInFrontOfProductRule(),
                    GroupTermsInSumRule()
                ),
                orderRules = listOf(
                    LexicographicalOrderRule()
                )
            )
        }

    }

    interface Rule {
        fun apply (expr: Expr): Expr
    }

}

fun Expr.simplify(options: Simplify.Options = Simplify.Options.Default) : Expr {
    var e = this
    for(rule in options.rules){
        e = rule.apply(e)
    }
    return e
}

fun Expr.simplify(vararg rules: Simplify.Rule) : Expr {
    var e = this
    for(rule in rules){
        e = rule.apply(e)
    }
    return e
}

fun Expr.ordered(options: Simplify.Options = Simplify.Options.Default) : Expr {
    var e = this
    for(rule in options.orderRules){
        e = rule.apply(e)
    }
    return e
}
