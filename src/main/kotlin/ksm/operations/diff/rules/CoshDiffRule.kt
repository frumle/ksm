package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule


object CoshDiffRule : DiffRule<Cosh> {
    override fun diff(e: Cosh, v: Variable, diffContext: DiffContext): Expr {
        return when(val d = diffContext.diff(e.expr, v)){
            Number.NULL -> Number.NULL
            Number.ONE -> sinh(e.expr)
            else -> multiply(sin(e.expr), d)
        }
    }
}
