package ksm.operations.diff.rules

import ksm.core.Number
import ksm.core.Variable
import ksm.core.base.Expr
import ksm.core.special.KroneckerDelta
import ksm.core.Vector
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object VariableDiffRule : DiffRule<Variable> {

    override fun diff(e: Variable, v: Variable, diffContext: DiffContext): Expr {
//        println("")
//        println("VarDiffRule: $e, $v")
//        println("VarDiffRule: ${e is Vector.Component} : ${e is Vector.Component}")
        return if(e.name == v.name) Number.ONE
        else {
            if(e is Vector.Component && v is Vector.Component){
                if(e.vector == v.vector) KroneckerDelta(e.index, v.index)
                else Number.NULL
            } else Number.NULL
        }
    }

}