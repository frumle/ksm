package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.utils.exceptions.NotSupportedException
import ksm.operations.diff.DiffRule

object PowerDiffRule : DiffRule<Power> {

    override fun diff(e: Power, v: Variable, diffContext: DiffContext): Expr {
        val db = diffContext.diff(e.base, v)
        val dp = diffContext.diff(e.pow, v)

        if(dp != Number.NULL && db != Number.NULL)
            throw NotSupportedException("Operation is not currently supported: both the base and power depend on $v.")

        return if(dp == Number.NULL){
            // power is constant
            when(db){
                Number.NULL -> Number.NULL
                Number.ONE -> multiply(e.pow, pow(e.base, e.pow - 1))
                else -> multiply(listOf(e.pow, pow(e.base, e.pow - 1), db))
            }
        } else {
            // base is constant
            when(dp){
                Number.NULL -> Number.NULL
                Number.ONE -> multiply(e, log(e.base))
                else -> multiply(listOf(e, log(e.base), dp))
            }
        }
    }

}
