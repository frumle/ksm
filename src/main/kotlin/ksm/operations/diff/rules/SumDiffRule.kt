package ksm.operations.diff.rules

import ksm.core.Number
import ksm.core.Variable
import ksm.core.base.Expr
import ksm.core.special.Sum
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object SumDiffRule : DiffRule<Sum> {

    override fun diff(e: Sum, v: Variable, diffContext: DiffContext): Expr {
        diffContext.reserve(e.index)
        val db = diffContext.diff(e.body, v)
        diffContext.release(e.index)
        return if(db != Number.NULL) Sum(e.index, e.lowerBound, e.upperBound, db)
        else Number.NULL
    }

}
