package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object CosDiffRule : DiffRule<Cos> {
    override fun diff(e: Cos, v: Variable, diffContext: DiffContext): Expr {
        return when(val d = diffContext.diff(e.expr, v)){
            Number.NULL -> Number.NULL
            Number.ONE -> Number.MINUS_ONE * sin(e.expr)
            else -> multiply(listOf(Number.MINUS_ONE, sin(e.expr), d))
        }
    }
}
