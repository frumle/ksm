package ksm.operations.diff.rules

import ksm.core.Number
import ksm.core.Variable
import ksm.core.add
import ksm.core.base.Expr
import ksm.core.function.Function
import ksm.core.function.FunctionDerivative
import ksm.core.function.FunctionValue
import ksm.core.times
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule


object FunctionDiffRule : DiffRule<FunctionValue> {

    override fun diff(e: FunctionValue, v: Variable, diffContext: DiffContext): Expr {
        val diffArgs = e.arguments.map { diffContext.diff(it, v) }

        // No argument depend on v
        if(diffArgs.all { it == Number.NULL })
            return Number.NULL

        val terms = mutableListOf<Expr>()
        diffArgs.forEachIndexed { index, dArg ->
            if(dArg != Number.NULL){
                val parameter = e.function.parameters[index] as Variable
                val df = e.function.diff(parameter).invoke(*e.arguments)
                terms += df * dArg
            }
        }

        val args = e.arguments
//        val terms = mutableListOf<Expr>()
//        args.forEach {
////            if()
//        }

        return if(terms.isNotEmpty()) {
            add(terms)
        } else Number.NULL
    }

    private fun Function.diff(v: Variable): Function {
        require(parameters.contains(v))
        return if(this is FunctionDerivative){
            val orders = this.orders.toMutableMap()
            orders[v] = (orders[v] ?: 0) + 1
            FunctionDerivative(this.function, orders)
        }else{
            FunctionDerivative(this, mapOf(v to 1))
        }
    }

//    override fun diff(e: FunctionValue, v: Variable, diffContext: DiffContext): Expr {
//        if(e.arguments.contains(v).not()) return Number.NULL
//        val of = e.playground.function
//        return if(of is FunctionDerivative){
//            val orders = of.orders.toMutableMap()
//            orders[v] = (orders[v] ?: 0) + 1
//            FunctionDerivative(of.playground.function, orders)
//        }else{
//            FunctionDerivative(of, mapOf(v to 1))
//        }.invoke(*e.arguments)  // As we should return derivative **value**
//    }
}
