package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object FractionDiffRule : DiffRule<Fraction> {
    override fun diff(e: Fraction, v: Variable, c: DiffContext): Expr {
        val dn = c.diff(e.numerator, v)
        val dd = c.diff(e.denominator, v)
        val p1 = if(dn != Number.NULL) dn / e.denominator else Number.NULL
        val p2 = if(dd != Number.NULL) (e.numerator / (e.denominator * e.denominator)) * dd else Number.NULL
        return p1 - p2
    }
}