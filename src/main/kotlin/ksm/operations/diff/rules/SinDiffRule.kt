package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object SinDiffRule : DiffRule<Sin> {

    override fun diff(e: Sin, v: Variable, diffContext: DiffContext): Expr {
        return when(val d = diffContext.diff(e.expr, v)){
            Number.NULL -> Number.NULL
            Number.ONE -> cos(e.expr)
            else -> cos(e.expr) * d
        }
    }

}
