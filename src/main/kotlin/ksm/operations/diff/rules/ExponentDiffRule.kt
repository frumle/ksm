package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object ExponentDiffRule : DiffRule<Exponent> {

    override fun diff(e: Exponent, v: Variable, diffContext: DiffContext): Expr {
        val d = diffContext.diff(e.expr, v)
        return when(d){
            Number.NULL -> Number.NULL
            Number.ONE -> exp(e.expr)
            else -> exp(e.expr) * d
        }
    }

}
