package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object MultiplicationDiffRule : DiffRule<Multiplication> {

    override fun diff(e: Multiplication, v: Variable, diffContext: DiffContext): Expr {
        val children = e.children.toMutableList()
        val dChildren = mutableListOf<Expr>()
        for(c in e.children){
            dChildren += diffContext.diff(c, v)
        }
        val prods = mutableListOf<Expr>()
        for(i in children.indices) {
            if(dChildren[i] != Number.NULL){
                val c = children[i]
                children[i] = dChildren[i]
                prods += multiply(children.toList())
                children[i] = c
            }
        }
        return when(prods.size) {
            0 -> Number.NULL
            1 -> prods.first()
            else -> add(prods)
        }
    }

}
