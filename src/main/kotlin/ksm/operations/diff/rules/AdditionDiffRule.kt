package ksm.operations.diff.rules

import ksm.core.Addition
import ksm.core.Number
import ksm.core.Variable
import ksm.core.add
import ksm.core.base.Expr
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule


object AdditionDiffRule : DiffRule<Addition> {

    override fun diff(e: Addition, v: Variable, diffContext: DiffContext): Expr {
        val children = mutableListOf<Expr>()
        for(c in e.children){
            val dc = diffContext.diff(c, v)
            if(dc != Number.NULL) children += dc
        }
        return when(children.size) {
            0 -> Number.NULL
            1 -> children.first()
            else -> add(children)
        }
    }

}
