package ksm.operations.diff.rules

import ksm.core.Number
import ksm.core.Variable
import ksm.core.base.Expr
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object ConstantDiffRule : DiffRule<Expr> {

    override fun diff(e: Expr, v: Variable, diffContext: DiffContext): Expr {
        return Number.NULL
    }

}
