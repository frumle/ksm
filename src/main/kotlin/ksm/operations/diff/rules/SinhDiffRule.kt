package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.operations.diff.DiffRule

object SinhDiffRule : DiffRule<Sinh> {
    override fun diff(e: Sinh, v: Variable, diffContext: DiffContext): Expr {
        return when(val d = diffContext.diff(e.expr, v)){
            Number.NULL -> Number.NULL
            Number.ONE -> cosh(e.expr)
            else -> multiply(cosh(e.expr), d)
        }
    }
}
