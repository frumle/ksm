package ksm.operations.diff.rules

import ksm.core.*
import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable
import ksm.operations.diff.DiffContext
import ksm.utils.exceptions.NotSupportedException
import ksm.operations.diff.DiffRule

object LogDiffRule : DiffRule<Log> {

    override fun diff(e: Log, v: Variable, diffContext: DiffContext): Expr {
        val db = diffContext.diff(e.base, v)
        val de = diffContext.diff(e.expr, v)

        if(db != Number.NULL && de != Number.NULL)
            throw NotSupportedException("Operation is not currently supported: both the base and power depend on $v.")

        return if(db == Number.NULL){
            // base is const
            when(de){
                Number.NULL -> Number.NULL
                Number.ONE -> multiply(log(e.base, Number.E), frac(Number.ONE, e.expr))
                else -> multiply(log(e.base, Number.E), frac(Number.ONE, e.expr), de)
            }
        }else{
            // expr is const
            val inv = frac(Number.ONE, log(e.expr, e.base))
            diffContext.diff(inv, v)
        }

    }

}
