package ksm.operations.diff

import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.special.KroneckerDelta
import ksm.operations.diff.rules.*
import kotlin.reflect.KClass

typealias RulesMutableMap = MutableMap<KClass<out Expr>, DiffRule<out Expr>>

object DiffRules {

    val defaultRules = diffRules { rules ->
        rules += VariableDiffRule
        rules += ConstantDiffRule
        rules += AdditionDiffRule
        rules += MultiplicationDiffRule
        rules += SinDiffRule
        rules += CosDiffRule
        rules += SinhDiffRule
        rules += CoshDiffRule
        rules += FractionDiffRule
        rules += ExponentDiffRule
        rules += PowerDiffRule
        rules += LogDiffRule
        rules += FunctionDiffRule
        rules += SumDiffRule
        rules.specialize<Number>(ConstantDiffRule)
        rules.specialize<KroneckerDelta>(ConstantDiffRule)
    }

}

fun diffRules(builder: (RulesMutableMap) -> Unit): DiffRulesMap {
    return mutableMapOf<KClass<out Expr>, DiffRule<out Expr>>().apply(builder)
}

inline operator fun <reified E: Expr> RulesMutableMap.plusAssign(rule: DiffRule<E>) {
    this[E::class] = rule
}

inline fun <reified E: Expr> RulesMutableMap.specialize(rule: DiffRule<Expr>) {
    this[E::class] = rule
}
