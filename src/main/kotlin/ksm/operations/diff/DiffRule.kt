package ksm.operations.diff

import ksm.core.base.Expr
import ksm.core.Variable

interface DiffRule <E: Expr> {
    fun diff(e: E, v: Variable, diffContext: DiffContext): Expr
}

