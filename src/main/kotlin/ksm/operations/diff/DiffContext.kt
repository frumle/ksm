package ksm.operations.diff

import ksm.core.Variable
import ksm.core.base.Expr
import ksm.core.base.Type
import java.lang.Exception
import kotlin.reflect.KClass

typealias DiffRulesMap = Map<KClass<out Expr>, DiffRule<out Expr>>

class DiffContext(private val rules: DiffRulesMap){

    private val _reserved = mutableSetOf<Variable>()

    fun diff(expr: Expr, variable: Variable): Expr {
        val rule = findRule(expr)
        return rule.diff(expr, variable, this)
    }

    val reservedVariables: Iterable<Variable> = _reserved

    fun reserve(variable: Variable){
        _reserved += variable
    }

    fun release(variable: Variable){
        _reserved -= variable
    }

    private fun findRule(e: Expr): DiffRule<Expr> {
        var rule = rules[e::class]
        if(rule == null){
            for(s in e::class.supertypes){
                rule = rules[s.classifier]
                if(rule != null) break
            }
        }

        if(rule == null)
            throw Exception("Diff rule not found for: (${e::class})")

        @Suppress("UNCHECKED_CAST")
        return rule as DiffRule<Expr>
    }

}

fun Expr.diff(variable: Variable, rules: DiffRulesMap = DiffRules.defaultRules): Expr {
    val context = DiffContext(rules)
    return context.diff(this, variable)
}

inline fun <R> DiffContext.withReserved(variable: Variable, block: DiffContext.()->R): R {
    reserve(variable)
    return block().also { release(variable) }
}

fun DiffContext.obtainFreeIndex(name: Char = 'i'): Variable {
    var number = -1
    val indices = mutableSetOf<Int>()
    reservedVariables.forEach {
        if(it.name[0] == name){
            if(it.name.length > 1){
                val indexNumber = it.name.substring(1).toIntOrNull() ?: -1
                if(indexNumber != -1)
                    indices += indexNumber
            } else indices += -1
        }
    }
    indices += 0 // artificially add 0 index to avoid 'i0' naming.
    indices.sorted().forEach {
        println(it)
        if(it > number) return@forEach
        if(it == number) number++
    }
    val numberedName = if(number == -1) name.toString() else "$name$number"
    return Variable(numberedName, Type.Integer)
}