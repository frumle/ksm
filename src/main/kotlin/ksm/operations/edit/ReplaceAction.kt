package ksm.operations.edit

import ksm.core.base.Expr

//fun Expr.replace(expr: Expr, with: Expr) : Expr {
//    return if(this == expr) with
//    else {
//        val replacedChildren = mutableListOf<Expr>()
//        for(child in children){
//            replacedChildren += child.replace(expr, with)
//        }
//        return copy(replacedChildren)
//    }
//}

class ReplaceContext {

    fun first(expr: Expr) : Pattern {
        TODO()
    }

    fun all(expr: Expr) : Pattern {
        TODO()
    }

    class Pattern {

    }

    fun Pattern.with(expr: Expr) {

    }

}

fun Expr.replace(session: ReplaceContext.() -> Unit) : Expr {
    val context = ReplaceContext()
    session.invoke(context)
    return TODO()
}




//fun Expr.replaceThroughMutable(expr: Expr, with: Expr) : Expr {
//    return MutableExpr.from(this).replace(expr, with).result()
//}

//fun MutableExpr.replace(expr: Expr, with: Expr) : MutableExpr {
//
//    return if (this.expr == expr){
//        this.also{ it.expr = with}
//    }
//    else {
//        val replacedChildren = mutableListOf<Expr>()
//        for (child in children) {
//            replacedChildren += child.replace(expr, with)
//        }
//        return copy(replacedChildren)
//    }
//}

//fun deepEquals(exprA: MutableExpr, exprB: Expr) : Boolean {
//    if(!exprA.expr.flatEquals(exprB)) return false
//
//}