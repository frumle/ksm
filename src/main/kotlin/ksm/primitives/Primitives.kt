package ksm.primitives

import ksm.core.base.Expr
import ksm.core.Number
import ksm.core.div
import ksm.core.plus
import ksm.core.times

operator fun <E: Expr> Int.times(expr: E) : Expr {
    return Number.int(this) * expr
}

operator fun <E: Expr> Int.plus(expr: E) : Expr {
    return Number.int(this) + expr
}

operator fun <E: Expr> Double.plus(expr: E) : Expr {
    return Number.real(this) + expr
}

operator fun <E: Expr> Double.times(expr: E) : Expr {
    return Number.real(this) + expr
}

operator fun <E: Expr> Double.div(expr: E) : Expr {
    return Number.real(this) / expr
}