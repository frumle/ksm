package ksm.utils

fun <E> List<E>.clone() : List<E> {
    return mutableListOf<E>().also { it.addAll(this) }
}
