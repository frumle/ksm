package ksm.utils

fun <T> Array<T>.hash(): Int {
    var hash = 0
    forEach { hash += 31 * hash + it.hashCode() }
    return hash
}

fun <T> Iterable<T>.hash(): Int {
    var hash = 0
    forEach { hash += 31 * hash + it.hashCode() }
    return hash
}

fun Int.plusHash(hash: Int): Int {
    return 31 * this + hash
}
