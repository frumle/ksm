package ksm.utils.exceptions

class NotSupportedException(message: String): Throwable(message)
