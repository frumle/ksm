package ksm.utils

import ksm.core.Number
import ksm.core.base.Expr

fun <E : Expr> MutableList<E>.containsZero() : Boolean {
    for(e in this){
        if(e == Number.NULL) return true
    }
    return false
}

fun <E : Expr> MutableList<E>.removeOnes() : Int {
    val it = this.iterator()
    var counter = 0
    while(it.hasNext()){
        if(it.next() == Number.ONE) it.remove().also { counter++ }
    }
    return counter
}

fun <E : Expr> MutableList<E>.removeMinusOnes() : Int {
    val it = this.iterator()
    var counter = 0
    while(it.hasNext()){
        if(it.next() == Number.MINUS_ONE) it.remove().also { counter++ }
    }
    return counter
}

