package ksm.utils

import ksm.core.Number
import ksm.core.base.Expr
import ksm.core.Variable

fun Expr.formatInParentheses(): String {
    return if(this is Variable || this is Number) this.toString() else "($this)"
}