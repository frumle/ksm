package ksm

import ksm.scope.MathScope
import ksm.core.Number
import ksm.core.Variable
import ksm.core.base.Expr
import ksm.core.base.Type
import ksm.core.special.Sum
import ksm.core.Vector
import ksm.scope.ExtendableMathScope
import ksm.scope.function.FunctionDefinitions
import ksm.scope.mathScope
import ksm.scope.variables.declarationInterceptor

fun math(notebook: MathScope.() -> Unit) {
    val scope = mathScope {
        add(FunctionDefinitions())
    }
    notebook.invoke(scope)
}

// Creation
fun MathScope.int(name: Char) : Variable {
    return variable(name.toString(), Type.Integer)
}

fun MathScope.int(name: String) : Variable {
    return variable(name, Type.Integer)
}

fun MathScope.int(name: Char, size: Expr) : Vector {
    return vector(name.toString(), Type.Integer, size)
}

fun MathScope.int(name: String, size: Expr) : Vector {
    return vector(name, Type.Integer, size)
}

fun MathScope.real(name: String) : Variable {
    return variable(name, Type.Real)
}

fun MathScope.real(name: Char) : Variable {
    return variable(name.toString(), Type.Real)
}

fun MathScope.real(name: Char, size: Int) : Vector {
    return vector(name.toString(), Type.Real, Number.int(size))
}

fun MathScope.real(name: Char, size: Expr) : Vector {
    return vector(name.toString(), Type.Real, size)
}

fun MathScope.real(name: Char, sizeVarName: Char) : Vector {
    return vector(name.toString(), Type.Real, int(sizeVarName))
}

fun MathScope.real(name: String, size: Int) : Vector {
    return vector(name, Type.Real, Number.int(size))
}

fun MathScope.real(name: String, size: Expr) : Vector {
    return vector(name, Type.Real, size)
}

fun MathScope.sum(index: String, lowerBound: Expr, upperBound: Expr, expr: (Variable)-> Expr) : Sum {
    val i = Variable(index, Type.Integer)
    return Sum(i, lowerBound, upperBound, expr.invoke(i))
}

fun MathScope.sum(index: String, lowerBound: Int, upperBound: Expr, expr: (Variable)-> Expr) : Sum {
    val i = Variable(index, Type.Integer)
    return Sum(i, Number.int(lowerBound), upperBound, expr.invoke(i))
}

fun MathScope.sum(index: String, lowerBound: Expr, upperBound: Int, expr: (Variable)-> Expr) : Sum {
    val i = Variable(index, Type.Integer)
    return Sum(i, lowerBound, Number.int(upperBound), expr.invoke(i))
}

fun MathScope.sum(index: String, lowerBound: Int, upperBound: Int, expr: (Variable)-> Expr) : Sum {
    val i = Variable(index, Type.Integer)
    return Sum(i, Number.int(lowerBound), Number.int(upperBound), expr.invoke(i))
}

// Internal
private fun MathScope.variable(name: String, type: Type): Variable {
    validateName(name)
    return Variable(name, type).also {
        declarationInterceptor?.declareVariable(it)
    }
}

private fun MathScope.vector(name: String, type: Type, size: Expr) : Vector {
    validateName(name)
    return Vector(name, type, size).also {
        declarationInterceptor?.declareVector(it)
    }
}

private fun validateName(name: String){
    require(!name.contains('_')) { "Variable name should not contain underscores!" }
}
