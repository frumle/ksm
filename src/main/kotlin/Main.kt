package playground

import ksm.core.*
import ksm.int
import ksm.math
import ksm.operations.diff.diff
import ksm.real
import ksm.sum

fun main(args: Array<String>){

    math {
        // Declare variables: real x, int n, and real vector y of size n
        val x = real('x')
        val n = int('n')
        val y = real('y', size = n)

        // Define symbolic expression using the declared variables
        val f = exp(sin(x)) + x * sum("i", 0, n) { i -> pow(x, i) * y[i] }
        println("f(x, y) = $f")

        // Differentiate the expression f wrt to variable x
        val dfx = f.diff(x)
        println("df(x, y) / dx = $dfx")

        // Differentiate the expression f wrt to variables x and y_j,
        // where j is the integer index of the vector component
        val dfxy = dfx.diff(y['j'])
        println("df(x, y) / dxdy = $dfxy")

        // TODO: generate C++ / Fortran code for the obtained expression
    }

}

