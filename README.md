ksm
===
Kotlin Symbolic Math (ksm) is intended to be the symbolic differentiation engine for analytical calculation of gradients of the scattering amplitudes in quantum field theory. This library can be extended with the user defined notations and correcponsing differentiation rules specific for their study. The final results of symbolic calculations can be exported, for example, as C++ or Fortran codes (TODO).

Motivation
--- 
The motivation for starting this project can be summarized as follows:

 - Provide an open source general purpose programming language interface for automatic differentiation problems. `Kotlin` is the perfect candidate for this task due to its typesystem, builtin support of functional data types and extension functions.
 - Analytically calculating the gradients of multivariate functions is not a strightforward problem even with modern tools like `Mathematica`, or `Maple`. Especially, when the expression is hard to represent in index-free matrix form.
 - In my research I needed the results of differentiation to be exported as executable codes (C++, Fortran) for further computations.
 - Finally, it is interesting, why not? :)

Example
---

- Start math scope
- Declare variables:
   - real scalar `x`
   - integer `n`
   - real vector `y` of size `n`
- Define expression `f`, composing these variables
- Differentiate the expression `f` with respect to the variable `x`
- Differentiate the expression `f` with respect to the variable `x`\
  and `j`-th component of the vector `y`, where `j` - is the integer index.

```
fun main(args: Array<String>){

    math {
        // Declare variables: real x, int n, and real vector y of size n
        val x = real('x')
        val n = int('n')
        val y = real('y', size = n)

        // Define symbolic expression using the declared variables
        val f = exp(sin(x)) + x * sum("i", 0, n) { i -> pow(x, i) * y[i] }
        println("f(x, y) = $f")

        // Differentiate the expression f wrt to variable x
        val dfx = f.diff(x)
        println("df(x, y) / dx = $dfx")

        // Differentiate the expression f wrt to variables x and y_j,
        // where j is the integer index of the vector component
        val dfxy = dfx.diff(y['j'])
        println("df(x, y) / dxdy = $dfxy")

        // TODO: generate C++ / Fortran code for the obtained expression
    }

}
```

TODO
---
 - Fix `simplify()`
 - Add `Fortran` and `C++` export feature